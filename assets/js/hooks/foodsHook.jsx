import React, {useReducer} from 'react'

function reducer(state, action) {
    switch (action.type) {
        case 'SET_FOODS':
            return {
                ...state, foods: {
                    data: action.payload.data['hydra:member'],
                    currentPage: action.payload.page,
                    totalItems: action.payload.data['hydra:totalItems'],
                    page: {
                        first: 'https://localhost:8000' + action.payload.data['hydra:view']['hydra:first'],
                        next: action.payload.data['hydra:view']['hydra:next'] ? 'https://localhost:8000' + action.payload.data['hydra:view']['hydra:next'] : null,
                        last: 'https://localhost:8000' + action.payload.data['hydra:view']['hydra:last'],
                    }
                }
            }
        case 'CREATE_FOOD':
            return {...state, foods: {data: [action.payload, ...state.foods]}}
        case 'UPDATE_FOOD':
            return {...state, foods: {data: state.foods.map(f => f.id === action.payload.id ? action.payload : f)}}
        case 'DELETE_FOOD':
            return {...state, foods: {data: state.foods.filter(f => f.id !== action.payload.id)}}
        default:
            throw new Error('Action ' + action.type + ' inconnue')
    }
}

export function useFoods() {
    const [state, dispatch] = useReducer(reducer, {
        foods: {
            data: null,
            currentPage: 1,
            totalItems: null,
            // Page first, next & last URL
            page: {
                first: null,
                next: null,
                last: null
            }
        }

    })

    return {
        foods: state.foods,
        fetchFoods: async (search = null, page = 1) => {
            const response = await fetch('https://localhost:8000/api/food?page=' + page + (search ? '?name=' + search : ''))

            const data = await response.json()

            dispatch({type: 'SET_FOODS', payload: {data, page}})
        },
        createFood: async (data) => {
            const response = await fetch('https://localhost:8000/api/food', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })

            const newFood = await response.json()

            dispatch({type: 'CREATE_FOOD', payload: newFood})
        },
        deleteFood: async (food) => {
            await fetch('https://localhost:8000/api/food/' + food.id, {
                method: 'DELETE'
            })
            dispatch({type: 'DELETE_FOOD', payload: food})
        },
        updateFood: async (food, data) => {
            const response = await fetch('https://localhost:8000/api/food/' + food.id, {
                method: 'PATCH',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/merge-patch+json'
                }
            })

            const updatedFood = await response.json()

            dispatch({type: 'UPDATE_FOOD', payload: updatedFood})
        }
    }
}
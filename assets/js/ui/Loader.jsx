import React from 'react'

export function Loader () {
    return <div className="spinner-border spinner-border-sm text-light ml-1" role="status">
        <span className="sr-only">Loading...</span>
    </div>
}
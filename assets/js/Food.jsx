import React, {useState} from 'react'
import {Loader} from './ui/Loader'

export function FoodList({foods, onCreate, onDelete, onUpdate, onSearch}) {
    const [addButton, setAddButton] = useState(false)
    const [food, setFood] = useState(null)
    const [searchLoader, setSearchLoader] = useState(false)

    const handleSearch = async (e) => {
        e.preventDefault()
        setSearchLoader(true)
        const formData = Object.fromEntries(new FormData(e.target))
        await onSearch(formData.search)
        setSearchLoader(false)
    }

    const handleFoodSubmit = async (e) => {
        e.preventDefault()
        const data = Object.fromEntries(new FormData(e.target))
        data.protein = parseFloat(data.protein)
        data.fat = parseFloat(data.fat)
        data.carbs = parseFloat(data.carbs)
        data.calories = parseFloat(data.calories)

        !food ? await onCreate(data) : await onUpdate(food, data)

        e.target.reset()
        setFood(null)
        setAddButton(false)
    }

    const handleFoodEdition = (food) => {
        setAddButton(false)
        setFood(food)
    }

    return <>
        {food && <FoodForm onSubmit={handleFoodSubmit} onCancel={setFood} food={food} submitButtonName="Edit"/>}

        {(!addButton && !food) &&
        <form onSubmit={handleSearch}>
            <div className="d-flex mb-2">
                <div className="flex-grow-1">
                    <input id="search" name="search" className="form-control"/>
                </div>
                <button type="submit" className="btn ml-1 btn-info">Search {searchLoader && <Loader/>}</button>
                <button type="button" className="btn ml-1 btn-success" onClick={() => setAddButton(v => !v)}>Add food</button>
            </div>
        </form>
        }

        {addButton && <FoodForm onSubmit={handleFoodSubmit} onCancel={setAddButton} submitButtonName="Add"/>}
        <table className="table">
            <thead className="bg-primary text-white">
            <tr>
                <td>Name</td>
                <td>Protein</td>
                <td>Fat</td>
                <td>Carbs</td>
                <td>Calories</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            {foods.data
                ? foods.data.map((v, k) => <FoodRow key={k} food={v} onDelete={onDelete} onEdition={handleFoodEdition}/>)
                : <tr></tr>
            }
            </tbody>
        </table>
        <nav aria-label="..." className="float-right">
            <ul className="pagination">
                {foods.currentPage > 1 && <li className="page-item">
                    <a className="page-link" href="#"  onClick={() => onSearch(null, foods.currentPage-1)}>Previous</a>
                </li>}
                <li className="page-item active">
                    <a className="page-link" href="#">{foods.currentPage} <span className="sr-only">(current)</span></a>
                </li>
                {foods.page.next  && <li className="page-item">
                    <a className="page-link" href='#' onClick={() => onSearch(null, foods.currentPage+1)}>Next</a>
                </li>}

            </ul>
        </nav>
    </>
}

/**
 * Food row a table
 */
function FoodRow({food, onDelete, onEdition}) {
    const [loader, setLoader] = useState(false)

    const handleDelete = async (food) => {
        setLoader(true)
        await onDelete(food)
        setLoader(false)
    }

    return <tr>
        <td>{food.name}</td>
        <td>{food.protein}</td>
        <td>{food.fat}</td>
        <td>{food.carbs}</td>
        <td>{food.calories}</td>
        <td>
            <button className="btn btn-sm btn-warning mr-1" onClick={() => onEdition(food)}>Editer</button>
            <button className="btn btn-sm btn-danger" onClick={() => handleDelete(food)}>
                Supprimer
                {loader && <Loader/>}
            </button>
        </td>
    </tr>
}

/**
 * Form allowing food creation & edition
 */
function FoodForm({onSubmit, onCancel, food, submitButtonName}) {
    const [loader, setLoader] = useState(false)

    const handleSubmit = async (e) => {
        setLoader(true)
        await onSubmit(e, food)
        setLoader(false)
    }


    return <form onSubmit={handleSubmit}>
        <div className="row">
            <div className="col-12">
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input name="name" id="name" className="form-control" defaultValue={food ? food.name : ''}/>
                </div>
            </div>
        </div>
        <div className="row">
            <div className="col-3">
                <div className="form-group">
                    <label htmlFor="protein">Protein</label>
                    <input name="protein" id="protein" className="form-control" defaultValue={food ? food.protein : ''}/>
                </div>
            </div>
            <div className="col-3">
                <div className="form-group">
                    <label htmlFor="fat">Fat</label>
                    <input name="fat" id="fat" className="form-control" defaultValue={food ? food.fat : ''}/>
                </div>
            </div>
            <div className="col-3">
                <div className="form-group">
                    <label htmlFor="carbs">Carbs</label>
                    <input name="carbs" id="carbs" className="form-control" defaultValue={food ? food.carbs : ''}/>
                </div>
            </div>
            <div className="col-3">
                <div className="form-group">
                    <label htmlFor="calories">Calories</label>
                    <input name="calories" id="calories" className="form-control" defaultValue={food ? food.calories : ''}/>
                </div>
            </div>
        </div>
        <div className="row">
            <div className="col-12">
                <button type="submit" className="btn btn-success mr-1">
                    {submitButtonName}
                    {loader && <Loader/>}
                </button>
                <button type="button" className="btn btn-secondary" onClick={() => onCancel(null)}>Cancel</button>
            </div>
        </div>
    </form>

}

import React from 'react'
import {TAB_INGREDIENTS, TAB_FOOD} from './consts'

export default function MainMenu({activeTab, handleTabClick}) {

    const tabClassName = (tab) => {
        let tabClassName = 'nav-item'
        if (tab === activeTab) tabClassName += ' active'

        return tabClassName
    }


    return <nav className="navbar navbar-dark bg-primary navbar-expand-lg">
        <a className="navbar-brand" href="#">NutriPro</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
                <li className={tabClassName(TAB_FOOD)}>
                    <a className="nav-link" href="#" onClick={() => handleTabClick(TAB_FOOD)}>Food</a>
                </li>
                <li className={tabClassName(TAB_INGREDIENTS)}>
                    <a className="nav-link" href="#" onClick={() => handleTabClick(TAB_INGREDIENTS)}>Ingredients</a>
                </li>
            </ul>
        </div>
    </nav>
}


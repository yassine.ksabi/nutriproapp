import MainMenu from './js/MainMenu'
import React, {useEffect} from 'react'
import ReactDOM from 'react-dom'
import {TAB_FOOD, TAB_INGREDIENTS} from './js/consts'
import {FoodList} from "./js/Food"
import {useFoods} from './js/hooks/foodsHook'

function App() {
    const [activeTab, setActiveTab] = React.useState(TAB_FOOD)

    const handleTabClick = (tab) => setActiveTab(tab)

    const {foods, fetchFoods, createFood, deleteFood, updateFood} = useFoods()

    useEffect(() => {
        switch (activeTab) {
            case TAB_FOOD:
                fetchFoods()
                break;
            case TAB_INGREDIENTS:
                break;
        }
    }, [activeTab])

    let content = null
    switch (activeTab) {
        case TAB_FOOD:
            content = <FoodList foods={foods} onUpdate={updateFood} onCreate={createFood} onDelete={deleteFood} onSearch={fetchFoods}/>
            break;
        case TAB_INGREDIENTS:
            break;
    }

    return <>
        <MainMenu activeTab={activeTab} handleTabClick={handleTabClick}/>
        <div className="container mt-4">
            {content}
        </div>
    </>
}

ReactDOM.render(<App/>, document.getElementById("js-root"))
